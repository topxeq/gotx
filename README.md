# GotX

A Golang interpreter based on [Yaegi](https://github.com/containous/yaegi).

Directly run Golang-like code files. Almost the same syntax as Golang. Installation of Golang is not required to run GotX code, it's convenient especially for running/debugging on remote servers.

Most of the standard libraries and some useful 3rd-party libraries are included.

Support GUI programming with OpenGL or LCL library.

A command-line only version (GotXc) is also available.

Currently supported 3rd-party libraies: github.com/topxeq/tk, github.com/topxeq/sqltk, github.com/beevik/etree, github.com/AllenDang/giu.


